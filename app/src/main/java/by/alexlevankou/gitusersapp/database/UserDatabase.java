package by.alexlevankou.gitusersapp.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import by.alexlevankou.gitusersapp.model.User;

@Database(entities = {User.class}, version = 3)
@TypeConverters({Converter.class})
public abstract class UserDatabase extends RoomDatabase {
    public abstract UserDao userDao();
}
