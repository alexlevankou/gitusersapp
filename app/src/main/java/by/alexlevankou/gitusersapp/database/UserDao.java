package by.alexlevankou.gitusersapp.database;

import android.arch.paging.DataSource;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;
import android.arch.persistence.room.Update;

import java.util.List;

import by.alexlevankou.gitusersapp.model.User;
import io.reactivex.Flowable;

@Dao
public abstract class UserDao {

    @Query("SELECT * FROM User")
    public abstract DataSource.Factory<Integer, User> getUsersDataSource();

    @Query("SELECT * FROM User WHERE login = :login")
    public abstract Flowable<User> getUser(String login);

    @Transaction
    public void refreshData(List<User> users) {
        deleteAll();
        insert(users);
    };

    @Query("DELETE FROM User")
    public abstract void deleteAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insert(List<User> users);

    @Update
    public abstract void update(User user);
}
