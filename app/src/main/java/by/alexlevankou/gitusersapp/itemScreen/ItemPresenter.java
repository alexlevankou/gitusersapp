package by.alexlevankou.gitusersapp.itemScreen;

import by.alexlevankou.gitusersapp.App;
import by.alexlevankou.gitusersapp.basemvp.BaseContract;
import by.alexlevankou.gitusersapp.basemvp.BasePresenter;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ItemPresenter extends BasePresenter<ItemFragmentView> implements BaseContract.Presenter {

    public void onActivityCreated(String login) {
        view.hideViews();
        view.showLoading();

        BaseContract.Model model = App.getInstance().getRepository();
        Disposable disposable = model.getUser(login)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(user -> {
                    if(user != null) {
                        if(user.isDetailed()){
                            view.showUser(user);
                            view.showViews();
                            view.hideLoading();
                        } else {
                            Disposable updateDisposable = model.updateUser(user.getLogin());
                            disposables.add(updateDisposable);
                        }
                    }
                });
        disposables.add(disposable);
    }
}
