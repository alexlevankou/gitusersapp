package by.alexlevankou.gitusersapp.itemScreen;


import by.alexlevankou.gitusersapp.basemvp.BaseContract;
import by.alexlevankou.gitusersapp.model.User;

public interface ItemFragmentView extends BaseContract.View {
    void showUser(User user);
    void hideViews();
    void showViews();
    void showLoading();
    void hideLoading();
}
