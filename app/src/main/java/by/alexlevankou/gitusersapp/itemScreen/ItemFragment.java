package by.alexlevankou.gitusersapp.itemScreen;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Locale;

import by.alexlevankou.gitusersapp.R;
import by.alexlevankou.gitusersapp.model.User;

public class ItemFragment extends Fragment implements ItemFragmentView {

    private FragmentActivity mActivity;

    private ConstraintLayout mLayout;
    private ImageView mAvatar;
    private TextView mName;
    private TextView mEmail;
    private TextView mOrganization;
    private TextView mFollowingCount;
    private TextView mFollowersCount;
    private TextView mDateCreated;

    private ProgressBar mProgressBar;

    public static ItemFragment newInstance() {
        return new ItemFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof FragmentActivity){
            mActivity =(FragmentActivity) context;
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.item_fragment, container, false);
        mLayout = view.findViewById(R.id.main_layout);
        mAvatar = view.findViewById(R.id.item_avatar);
        mName = view.findViewById(R.id.item_name);
        mEmail = view.findViewById(R.id.email);
        mOrganization = view.findViewById(R.id.organization);
        mFollowingCount = view.findViewById(R.id.following_count);
        mFollowersCount = view.findViewById(R.id.followers_count);
        mDateCreated = view.findViewById(R.id.date);

        mProgressBar = view.findViewById(R.id.progressBar);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Bundle extras = getArguments();
        if(extras != null) {
            String login = extras.getString(getResources().getString(R.string.login));
            ItemPresenter presenter = ViewModelProviders.of(mActivity).get(ItemPresenter.class);
            presenter.attachView(this, getLifecycle());
            presenter.onActivityCreated(login);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }

    @Override
    public void showUser(User user) {
        mName.setText(user.getName());
        mEmail.setText(user.getEmail());
        mOrganization.setText(user.getOrganization());
        mFollowingCount.setText(String.valueOf(user.getFollowingCount()));
        mFollowersCount.setText(String.valueOf(user.getFollowersCount()));
        if(user.getCreatedDate() != null){
            Format formatter = new SimpleDateFormat("dd/MM/YYYY", Locale.getDefault());
            StringBuilder sb = new StringBuilder();
            sb.append(getResources().getString(R.string.date_created));
            sb.append(" ");
            sb.append(formatter.format(user.getCreatedDate()));
            mDateCreated.setText(sb.toString());
        }

        Glide
            .with(getActivity())
            .load(user.getAvatarUrl())
            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
            .placeholder(R.drawable.ic_download)
            .error(R.drawable.ic_no_connection)
            .fallback(R.drawable.ic_empty)
            .into(mAvatar);
    }

    @Override
    public void showViews(){
        mLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideViews(){
        mLayout.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showLoading() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        mProgressBar.setVisibility(View.GONE);
    }
}
