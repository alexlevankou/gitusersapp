package by.alexlevankou.gitusersapp.basemvp;

import android.arch.lifecycle.LiveData;
import android.arch.paging.DataSource;
import android.arch.paging.PagedList;

import by.alexlevankou.gitusersapp.model.User;
import io.reactivex.Flowable;
import io.reactivex.disposables.Disposable;

public class BaseContract {

    public interface View {
    }

    public interface Presenter {
    }

    public interface Model {
        Disposable updateUser(String login);
        Flowable<User> getUser(String login);
        LiveData<PagedList<User>> getPagedList();
        DataSource.Factory<Integer, User> getUsersDataSource();
        Disposable refreshData();
    }
}
