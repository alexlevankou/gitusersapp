package by.alexlevankou.gitusersapp.listScreen;

import android.arch.paging.PagedList;

import by.alexlevankou.gitusersapp.basemvp.BaseContract;
import by.alexlevankou.gitusersapp.model.User;


public interface ListFragmentView extends BaseContract.View {
    void showUsers(PagedList<User> users);
    void showNoDataText();
    void stopRefreshing();
}
