package by.alexlevankou.gitusersapp.listScreen;

import android.arch.lifecycle.LiveData;
import android.arch.paging.PagedList;

import by.alexlevankou.gitusersapp.App;
import by.alexlevankou.gitusersapp.basemvp.BaseContract;
import by.alexlevankou.gitusersapp.basemvp.BasePresenter;
import by.alexlevankou.gitusersapp.model.User;
import io.reactivex.disposables.Disposable;

public class ListPresenter extends BasePresenter<ListFragmentView> implements BaseContract.Presenter  {

    private boolean isRefreshing = false;

    LiveData<PagedList<User>> getPagedList(){
        return App.getInstance().getRepository().getPagedList();
    }

    void onRefresh() {
        isRefreshing = true;
        Disposable refreshDisposable = App.getInstance().getRepository().refreshData();
        disposables.add(refreshDisposable);
    }

    void onDatasetChanged(PagedList<User> users){
        if(isRefreshing){
            view.stopRefreshing();
            isRefreshing = false;
        }
        if(users != null && users.size() > 0){
            view.showUsers(users);
        } else {
            view.showNoDataText();
        }
    }
}
