package by.alexlevankou.gitusersapp.listScreen.adapter;

import android.arch.paging.PagedListAdapter;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import by.alexlevankou.gitusersapp.R;
import by.alexlevankou.gitusersapp.listScreen.ListFragment;
import by.alexlevankou.gitusersapp.model.User;


public class PagingAdapter extends PagedListAdapter<User, ViewHolder> {

    private final ListFragment.OnListFragmentInteractionListener mListener;
    private SwipeRefreshLayout refreshLayout;

    public PagingAdapter(UserDiffUtilCallback diffUtilCallback, ListFragment.OnListFragmentInteractionListener listener, SwipeRefreshLayout refreshLayout) {
        super(diffUtilCallback);
        mListener = listener;
        this.refreshLayout = refreshLayout;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new ViewHolder(view, mListener, refreshLayout);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        User item = getItem(position);
        if(item != null){
            holder.bindItem(item);
        }
    }
}