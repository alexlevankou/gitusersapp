package by.alexlevankou.gitusersapp.listScreen.adapter;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import by.alexlevankou.gitusersapp.R;
import by.alexlevankou.gitusersapp.listScreen.ListFragment;
import by.alexlevankou.gitusersapp.model.User;

class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    private ListFragment.OnListFragmentInteractionListener itemListener;
    private SwipeRefreshLayout refreshLayout;
    private Context context;
    private final ImageView avatarImage;
    private final TextView loginText;
    private final TextView idText;
    private User user;

    ViewHolder(View view, ListFragment.OnListFragmentInteractionListener itemListener, SwipeRefreshLayout refreshLayout) {
        super(view);
        view.setOnClickListener(this);
        this.refreshLayout = refreshLayout;
        this.itemListener = itemListener;
        context = view.getContext();
        avatarImage = view.findViewById(R.id.avatar);
        loginText = view.findViewById(R.id.login);
        idText = view.findViewById(R.id.user_id);
    }

    void bindItem(User user){
        this.user = user;
        loginText.setText(user.getLogin());
        idText.setText(String.valueOf(user.getId()));
        Glide
            .with(context)
            .load(user.getAvatarUrl())
            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
            .placeholder(R.drawable.ic_download)
            .error(R.drawable.ic_no_connection)
            .fallback(R.drawable.ic_empty)
            .into(avatarImage);
    }

    @Override
    public void onClick(View v){
        if(!refreshLayout.isRefreshing()){
            itemListener.onListFragmentInteraction(user.getLogin());
        }
    }
}
