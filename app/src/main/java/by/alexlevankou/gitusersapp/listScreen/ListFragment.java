package by.alexlevankou.gitusersapp.listScreen;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModelProviders;
import android.arch.paging.PagedList;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import by.alexlevankou.gitusersapp.R;
import by.alexlevankou.gitusersapp.listScreen.adapter.PagingAdapter;
import by.alexlevankou.gitusersapp.listScreen.adapter.UserDiffUtilCallback;
import by.alexlevankou.gitusersapp.model.User;


public class ListFragment extends Fragment implements ListFragmentView {

    private FragmentActivity mActivity;
    private ListPresenter presenter;
    private PagingAdapter pagingAdapter;
    private OnListFragmentInteractionListener mListener;

    private RecyclerView mRecyclerView;
    private TextView mNoDataText;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private Parcelable lastLayoutManagerState;

    public static ListFragment newInstance() {
        return new ListFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof FragmentActivity){
            mActivity =(FragmentActivity) context;
        }
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + getResources().getString(R.string.list_fragment_interaction_interface_not_implemented));
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_fragment, container, false);
        mRecyclerView = view.findViewById(R.id.list);
        mNoDataText = view.findViewById(R.id.no_data_text);
        mSwipeRefreshLayout = view.findViewById(R.id.swipe_container);

        if (mRecyclerView != null) {
            Context context = view.getContext();
            mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        }
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        presenter = ViewModelProviders.of(mActivity).get(ListPresenter.class);
        presenter.attachView(this, getLifecycle());
        setPagingAdapter();
        mSwipeRefreshLayout.setOnRefreshListener(() -> presenter.onRefresh());
        if (savedInstanceState != null) {
            lastLayoutManagerState = savedInstanceState.getParcelable("layout_manager_state");
        }
    }

    private void setPagingAdapter() {
        pagingAdapter = new PagingAdapter(new UserDiffUtilCallback(), mListener, mSwipeRefreshLayout);
        LiveData<PagedList<User>> pagedListLiveData = presenter.getPagedList();
        pagedListLiveData.observe(this, users -> presenter.onDatasetChanged(users));
        mRecyclerView.setAdapter(pagingAdapter);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putParcelable("layout_manager_state", mRecyclerView.getLayoutManager().onSaveInstanceState());
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
        mListener = null;
    }

    @Override
    public void showUsers(PagedList<User> users) {
        pagingAdapter.submitList(users);
        if(lastLayoutManagerState != null) {
            mRecyclerView.getLayoutManager().onRestoreInstanceState(lastLayoutManagerState);
            lastLayoutManagerState = null;
        }
        mRecyclerView.setVisibility(View.VISIBLE);
        mNoDataText.setVisibility(View.GONE);
    }

    @Override
    public void showNoDataText() {
        mRecyclerView.setVisibility(View.GONE);
        mNoDataText.setVisibility(View.VISIBLE);
    }

    @Override
    public void stopRefreshing(){
        mSwipeRefreshLayout.setRefreshing(false);
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(String login);
    }
}
