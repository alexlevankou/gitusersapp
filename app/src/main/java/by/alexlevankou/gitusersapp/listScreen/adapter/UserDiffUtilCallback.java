package by.alexlevankou.gitusersapp.listScreen.adapter;


import android.support.v7.util.DiffUtil;

import by.alexlevankou.gitusersapp.model.User;

public class UserDiffUtilCallback extends DiffUtil.ItemCallback<User> {

    @Override
    public boolean areItemsTheSame(User oldUser, User newUser) {
        return oldUser.getId() == newUser.getId();
    }

    @Override
    public boolean areContentsTheSame(User oldUser, User newUser) {
        return oldUser.getLogin().equals(newUser.getLogin())
                && oldUser.getAvatarUrl().equals(newUser.getAvatarUrl());
    }
}