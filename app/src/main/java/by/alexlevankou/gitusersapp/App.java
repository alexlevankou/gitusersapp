package by.alexlevankou.gitusersapp;

import android.app.Application;
import android.arch.persistence.room.Room;

import by.alexlevankou.gitusersapp.data.Repository;
import by.alexlevankou.gitusersapp.database.UserDatabase;

public class App extends Application {

    private static App mInstance;
    private Repository mRepository;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        UserDatabase mDatabase = Room.databaseBuilder(this, UserDatabase.class, "user_database")
                .fallbackToDestructiveMigration()
                .build();
        mRepository = new Repository(mDatabase.userDao());
    }

    public static App getInstance() {
        return mInstance;
    }

    public Repository getRepository() {
        return mRepository;
    }
}
