package by.alexlevankou.gitusersapp.network;


import java.util.List;

import by.alexlevankou.gitusersapp.model.User;
import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface GitHubApi {

    @GET("/users")
    Observable<List<User>> getUsers(@Query("since") int since, @Query("per_page") int perPage);

    @GET("/users")
    Call<List<User>> callUsers(@Query("since") int since, @Query("per_page") int perPage);

    @GET("/users/{login}")
    Single<User> getUser(@Path("login") String login);
}
