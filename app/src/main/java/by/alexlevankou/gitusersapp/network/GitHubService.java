package by.alexlevankou.gitusersapp.network;


import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import by.alexlevankou.gitusersapp.BuildConfig;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public final class GitHubService {

    private static GitHubService mInstance;
    private String apiUrl;
    private String apiKey;
    private Retrofit mRetrofit;


    private GitHubService() {
        apiUrl = BuildConfig.GITHUB_API;
        apiKey = BuildConfig.GITHUB_API_KEY;
        buildRetrofit();
    }

    public static GitHubService getInstance() {
        if (mInstance == null) {
            mInstance = new GitHubService();
        }
        return mInstance;
    }

    private void buildRetrofit() {
        Gson gson = new GsonBuilder().setLenient().create();
        mRetrofit = new Retrofit.Builder()
                .baseUrl(apiUrl)
                .client(buildClient(apiKey))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    @NonNull
    private static OkHttpClient buildClient(String apiKey) {
        return new OkHttpClient.Builder()
                .addInterceptor(new HttpLoggingInterceptor())
                .addInterceptor(new ApiKeyInterceptor(apiKey))
                .build();
    }

    public GitHubApi getApi() {
        return mRetrofit.create(GitHubApi.class);
    }
}
