package by.alexlevankou.gitusersapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import by.alexlevankou.gitusersapp.basemvp.BaseContract;
import by.alexlevankou.gitusersapp.itemScreen.ItemFragment;
import by.alexlevankou.gitusersapp.listScreen.ListFragment;


public class MainActivity extends AppCompatActivity implements BaseContract.View, ListFragment.OnListFragmentInteractionListener {

    private static Fragment.SavedState savedState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        if (savedInstanceState == null) {
            showFragment(ListFragment.newInstance(), getResources().getString(R.string.list_fragment_tag));
        }
    }

    @Override
    public void onBackPressed() {
        Fragment itemFragment = getSupportFragmentManager().findFragmentByTag(getResources().getString(R.string.item_fragment_tag));
        if (itemFragment != null && itemFragment.isVisible()) {
            ListFragment listFragment = ListFragment.newInstance();
            if(savedState != null){
                listFragment.setInitialSavedState(savedState);
            }
            showFragment(listFragment, getResources().getString(R.string.list_fragment_tag));
        } else {
            finishAndRemoveTask();
        }
    }

    @Override
    public void onListFragmentInteraction(String login) {
        Fragment listFragment = getSupportFragmentManager().findFragmentByTag(getResources().getString(R.string.list_fragment_tag));
        savedState = getSupportFragmentManager().saveFragmentInstanceState(listFragment);
        Bundle bundle = new Bundle();
        bundle.putString(getResources().getString(R.string.login), login);
        Fragment itemFragment = ItemFragment.newInstance();
        itemFragment.setArguments(bundle);
        showFragment(itemFragment, getResources().getString(R.string.item_fragment_tag));
    }

    private void showFragment(Fragment fragment, String tag) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, fragment, tag)
                .commit();
    }
}
