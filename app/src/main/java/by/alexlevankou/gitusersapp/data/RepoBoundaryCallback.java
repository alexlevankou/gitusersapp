package by.alexlevankou.gitusersapp.data;

import android.arch.paging.PagedList;
import android.support.annotation.NonNull;

import java.util.List;

import by.alexlevankou.gitusersapp.model.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RepoBoundaryCallback extends PagedList.BoundaryCallback<User> {

    private static final int NETWORK_PAGE_SIZE = 40;

    private Repository repository;
    private boolean isRequestInProgress = false;

    RepoBoundaryCallback(Repository repository) {
        this.repository = repository;
    }

    @Override
    public void onZeroItemsLoaded() {
        requestAndSaveData(0);
    }

    @Override
    public void onItemAtEndLoaded(@NonNull User itemAtEnd) {
        requestAndSaveData(itemAtEnd.getId());
    }

    private void requestAndSaveData(int since) {
        if (isRequestInProgress) return;

        isRequestInProgress = true;

        repository.callNextPage(since, NETWORK_PAGE_SIZE).enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(@NonNull Call<List<User>> call, @NonNull Response<List<User>> response) {
                isRequestInProgress = false;
                Thread t = new Thread(() -> repository.insert(response.body()));
                t.start();
            }

            @Override
            public void onFailure(@NonNull Call<List<User>> call, @NonNull Throwable t) {
                isRequestInProgress = false;
                t.printStackTrace();
            }
        });
    }
}

