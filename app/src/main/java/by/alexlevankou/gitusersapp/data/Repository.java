package by.alexlevankou.gitusersapp.data;

import android.arch.lifecycle.LiveData;
import android.arch.paging.DataSource;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PagedList;

import java.util.List;
import java.util.concurrent.Executors;

import by.alexlevankou.gitusersapp.basemvp.BaseContract;
import by.alexlevankou.gitusersapp.database.UserDao;
import by.alexlevankou.gitusersapp.model.User;
import by.alexlevankou.gitusersapp.network.GitHubApi;
import by.alexlevankou.gitusersapp.network.GitHubService;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;


public class Repository implements BaseContract.Model {

    private UserDao mUserDao;
    private static final int DATABASE_PAGE_SIZE = 20;
    private static final int DATABASE_START_PAGE_SIZE = 40;
    private GitHubApi gitHubApi;

    public Repository(UserDao userDao) {
        this.mUserDao = userDao;
        gitHubApi = GitHubService.getInstance().getApi();
    }

    @Override
    public Flowable<User> getUser(String login) {
        return mUserDao.getUser(login);
    }

    @Override
    public DataSource.Factory<Integer, User> getUsersDataSource() {
        return mUserDao.getUsersDataSource();
    }

    @Override
    public Disposable updateUser(String login) {
        return gitHubApi
                .getUser(login)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(
                    user -> { user.setDetailed(true); mUserDao.update(user); },
                    Throwable::printStackTrace
                );
    }

    @Override
    public Disposable refreshData() {
        return loadNextPage(0,DATABASE_START_PAGE_SIZE)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(mUserDao::refreshData);
    }

    @Override
    public LiveData<PagedList<User>> getPagedList() {
        PagedList.Config config = new PagedList.Config.Builder()
            .setEnablePlaceholders(true)
            .setPageSize(DATABASE_PAGE_SIZE)
            .build();
        return new LivePagedListBuilder<>(getUsersDataSource(), config)
                .setFetchExecutor(Executors.newSingleThreadExecutor())
                .setBoundaryCallback(new RepoBoundaryCallback(this))
                .build();
    }

    void insert(List<User> users){ mUserDao.insert(users); }

    private Observable<List<User>> loadNextPage(int since, int perPage) {
        return gitHubApi.getUsers(since, perPage);
    }

    Call<List<User>> callNextPage(int since, int perPage) {
        return gitHubApi.callUsers(since, perPage);
    }
}
